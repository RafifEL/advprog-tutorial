package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;
import java.util.List;

public class ChainSpell implements Spell {
    List<Spell> spells;
    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }
    // TODO: Complete Me


    @Override
    public void cast() {
        for (Spell spell : spells){
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for(int i = spells.size()-1; i >= 0; i--){
            spells.get(i).cast();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
